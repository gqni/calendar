# calender

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

# 使用方法

# install
>npm i vue-calendar-component --save

>cnpm i vue-calendar-component --save  //国内镜像

## 代码

```
<template>
    <div class="calender">
        <Calendar
      v-on:choseDay="clickDay"
      v-on:changeMonth="changeDate"
    ></Calendar>
    </div>
</template>

<script>
import Calendar from 'vue-calendar-component'
export default {
     components: {
        Calendar
     },
     methods :{
         clickDay(data) {
          console.log(data); //选中某天
        },
        changeDate(data) {
          console.log(data); //左右点击切换月份
        },
        clickToday(data) {
          console.log(data); //跳到了本月
        }
     }
}
</script>
<style>
.calender .wh_content_all{
    background: #fff;
}
.calender .wh_top_changge{
    background: #fcc805;
}
.calender .wh_top_changge li{
  color: #fff;
}
.calender .wh_content_item, .wh_content_item_tag{
    color:#666
}
.calender .wh_content_item .wh_isToday{
    background: #fcc805;
    color:#fff
}
.calender .wh_content_item .wh_chose_day {
    background: #eee;
}
</style>
```

## 效果图

![输入图片说明](https://images.gitee.com/uploads/images/2018/0806/104435_ad154722_2011549.png "屏幕截图.png")
