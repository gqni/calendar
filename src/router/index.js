import Vue from 'vue'
import Router from 'vue-router'
import calender from '@/components/calender'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'calender',
      component: calender
    }
  ]
})
